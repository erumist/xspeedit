let XspeedIt = require('./xspeedit')


const formatOutput = (items) => {
   return items.map(item => item.join('')).join('/');
}

const formatInput = (string) => {
    return string.split('').map(item => parseInt(item));
}

try {
    const xspeedit = new XspeedIt();
    const argv = process.argv;

    // Check we have only 3 parameters
    if (argv.length !== 3) {
        throw new Error('One argument is required (and only one)');
    }

    console.log('Articles : ' + argv[2]);

    const formattedInput = formatInput(argv[2]); 
    const output = xspeedit.calculate(formattedInput);

    console.log('Articles emballés: ' + formatOutput(output));
}
catch(e){
    console.log(e.message);
}