var assert = require('assert');
var chai = require('chai');
var expect = chai.expect;
var XspeedIt = require('../xspeedit/xspeedit');

describe("XspeedIt", function() {
  describe("calculate", function() {
    let xspeedit = new XspeedIt();

    it("should throw an error when value is not an array 1/2", function() {
      const arg = "qwerty";
      expect(() => xspeedit.calculate(arg)).to.throw(XspeedIt.ERRORS.NOT_AN_ARRAY);
    });

    it("should throw an error when value is not an array 2/2", function() {
      const arg = null;
      expect(() => xspeedit.calculate(arg)).to.throw(XspeedIt.ERRORS.NOT_AN_ARRAY);
    });

    it("should throw an error when array is empty", function() {
      const arg = [];
      expect(() => xspeedit.calculate(arg)).to.throw(XspeedIt.ERRORS.EMPTY_ARRAY);
    });

    it("should throw an error when array not contains number 1/2", function() {
      const arg = ["null"];
      expect(() => xspeedit.calculate(arg)).to.throw(XspeedIt.ERRORS.NOT_A_NUMBER);
    });

    it("should throw an error when array not contains number 2/2", function() {
      const arg = [1, 2, 3, "q"];
      expect(() => xspeedit.calculate(arg)).to.throw(XspeedIt.ERRORS.NOT_A_NUMBER);
    });

    it("should throw an error when array contains 0", function() {
      const arg = [1, 2, 3, 4, 0];
      expect(() => xspeedit.calculate(arg)).to.throw(XspeedIt.ERRORS.NOT_A_NUMBER);
    });

    it("should return 8 bags", function() {
      const arg = [1, 6, 3, 8, 4, 1, 6, 8, 9, 5, 2, 5, 7, 7, 3];
      expect(xspeedit.calculate(arg)).to.have.length(8);
    });

    it("should throw an error if max height is lower than 9", function() {
      expect(() => new XspeedIt(8)).to.throw(XspeedIt.ERRORS.MAX_WEIGHT_TOO_SMALL);
    });

  });
});
