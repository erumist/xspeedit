module.exports = class XspeedIt {


  constructor(maxWeight = 10) {
    if (maxWeight < 9) {
      throw new Error(XspeedIt.ERRORS.MAX_WEIGHT_TOO_SMALL);
    }
    this.maxWeight = maxWeight;
  }

  static get ERRORS() {
    return {
      NOT_AN_ARRAY: "Items supplied should be an array.",
      NOT_A_NUMBER: "Type of array supplied should be number and different of 0.",
      EMPTY_ARRAY: "Items supplied is empty.",
      MAX_WEIGHT_TOO_SMALL: "Max weight of the list too low. Should be >= 9."
    };
  }

  /**
   *
   *
   * @param {Number[]} items: list of items to organize
   * @returns {Number[][]} : bag of items organized
   * @memberof XspeedIt
   */
  calculate(items) {
    // Check if items is an array
    if (!Array.isArray(items)) {
      throw new Error(XspeedIt.ERRORS.NOT_AN_ARRAY);
    }
    // Check if items is not empty
    if (items.length === 0) {
        throw new Error(XspeedIt.ERRORS.EMPTY_ARRAY);
    }
    // Check that each item is a number
    items.forEach(item => {
      if (typeof item !== "number" || isNaN(item) || item === 0) {
        throw new Error(XspeedIt.ERRORS.NOT_A_NUMBER);
      }
    });

    // Sort decrease the array
    items = items.sort((a, b) => b - a);
    const bags = [];

    // Try to first item (biggest of the sorted list) to the latest one (lowest of the sorted list) 
    while (items.length > 0) {
      const firstItem = items.shift();
      let weight = firstItem;
      let bag = [firstItem];
      let isBagFull = false;
      // While bag is not full, attach lowest item to the bag
      while (!isBagFull) {
        if (
          items.length > 0 &&
          this.maxWeight - items[items.length - 1] - weight >= 0
        ) {
          const lastItem = items.pop();
          bag.push(lastItem);
          weight += lastItem; // we could use bag.reduce but it would be slower
        } else {
          isBagFull = true;
        }
      }
      bags.push(bag);
    }
    return bags;
  }
};
