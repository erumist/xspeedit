XspeedIt
========

Une version du test XspeedIt. 
Peut être optimiser en ré-implémentant l'algorithme de trie, le reste du calcul se faisant en O(n). (La liste triée est parcourue une seule fois)


# Install
```npm install```

# Execution
```npm start 12315648974```

# Test
```npm run test```